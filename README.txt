# Smartphone based recognition of human Activities

Note:This repository houses the comprehensive study and implementation of human activity recognition using smartphone sensors. Three distinct neural network models were trained and evaluated for this purpose, and the findings were subsequently integrated into two Android applications.

## Required packages

* !pip install --upgrade setuptools pip
* !pip install seaborn
* !pip install keras
* !pip install numpy
* !pip install matplotlib
* !pip3 install pyunpack
* !pip3 install patool
* !pip install pytz
* !pip install pandas
* !pip install scikit-learn
* !pip install tensorflow


1. Dataset Overview
The dataset comprises sensor readings from the Accelerometer, Linear Acceleration Sensor, and Gyroscope sensor. Each data point is timestamped and labeled with the corresponding activity. The dataset encompasses the following activities: 

*Walking
*Standing
*Jogging
*Sitting
*Biking
*Upstairs
*Downstairs
You can download the dataset from this link: https://www.utwente.nl/en/eemcs/ps/dataset-folder/sensors-activity-recognition-dataset-shoaib.rar.

2. Data Visualization

Visualizations images can be seen in images folder represent sensor data on the X-Y-Z axis respective to each activity.

3. Data Preprocessing Steps:

*Merged data from 10 files (collected by 10 individuals) into a single file.
*Segregated the dataset into Training and Validation sets.
*Encoded activity labels into numerical format.
*Converted sensor data into Time Series data for efficient processing

4. Models Overview:
We explored three models for this project:

*Model 1: 1D CNN

This Convolutional Neural Network was designed to efficiently process sequence data by sliding a kernel over the input data. Comprising multiple convolutional layers, the 1D CNN was able to detect local patterns in the sequences.

*Model 2: FNN (Feedforward Neural Network)
The Feedforward Neural Network, a traditional deep learning model, was implemented with multiple dense layers. While it lacks the memory feature of LSTMs, its simplicity enabled rapid training and provided a baseline performance for activity recognition.

*Model 3: LSTM (Long Short-Term Memory)
LSTMs, a category of recurrent neural networks (RNNs), are adept at processing sequences due to their inherent memory feature. The LSTM model was structured with several layers to capture the temporal dependencies in the sensor data.

Each model utilized Categorical Crossentropy for loss measurement and the Adam (Adaptive Momentum) optimizer. All models underwent training for 5 epochs, utilizing the ModelCheckpoint callback to retain the most accurate iteration.

6. Application Deployment:
Two tailored applications were developed to leverage our trained models:

*HAR Recorder App: This application records data and calculates distances based on human activity.

*Human Activity Recognition App: It identifies human activities in real-time, and upon detection, communicates them audibly via voice command.
